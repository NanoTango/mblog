from django.shortcuts import render
from django.http import HttpResponse, Http404
from .models import Post

# Create your views here.
def homepage(request):
    posts = Post.objects.all()
    post_list = list()
    for count, post in enumerate(posts):
        post_list.append("No.{}:".format(str(count)) + str(post) + "<br/>")
    return HttpResponse(post_list)


def detail(request, post_id):
    try:
        post = Post.objects.get(pk=post_id)
        html = '<h1>' + post.title + '</h1>'
        html += '<p>' + post.body.encode('utf-8') + '</p>'
        return HttpResponse(html)
    except Post.DoesNotExist:
        raise Http404('Post does not exist!!')
    
