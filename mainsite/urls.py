from django.urls import path

from . import views

urlpatterns = [
    path('', views.homepage),
    path('<int:post_id>/', views.detail)
]